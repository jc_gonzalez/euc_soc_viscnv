/******************************************************************************
 * File:    config_tables.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.ConfigTables
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement ConfigTables class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "config_tables.h"

//----------------------------------------------------------------------
// Constructor
// Initialize class instance
//----------------------------------------------------------------------
ConfigTables::ConfigTables() :
    cfgSine(1), cfgFreq(2), cfgShortFreq(1)
{
}

//----------------------------------------------------------------------
// Method: read
// Takes the content of the object from the binary stream
//----------------------------------------------------------------------
bool ConfigTables::read(ifstream & fh)
{
    bool retval = true;
    UInt16 data;

    retval &= data.read(fh);
    if (retval) { cfgSine = data.value; }

    retval &= data.read(fh);
    if (retval) { cfgFreq = data.value; }

    retval &= data.read(fh);
    if (retval) { cfgShortFreq = data.value; }

    return retval;
}

//----------------------------------------------------------------------
// Method: info
// String with the representation of the object
//----------------------------------------------------------------------
string ConfigTables::info()
{
    stringstream ss("");
    ss << cfgSine << ", " << cfgFreq << ", " << cfgShortFreq;
    return ss.str();
}
