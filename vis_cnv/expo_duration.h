/******************************************************************************
 * File:    expo_duration.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.ExposureDuration
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare ExposureDuration class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef EXPOSUREDURATION_H
#define EXPOSUREDURATION_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"

//==========================================================================
// Class: ExposureDuration
// Contains fields with ROE Exposure duration fields
//==========================================================================
class ExposureDuration {

public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
    ExposureDuration();

public:
    UInt64 expDuration[12];

public:
    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);

    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // EXPOSUREDURATION_H
