/******************************************************************************
 * File:    expo_duration.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.ExposureDuration
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement ExposureDuration class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "expo_duration.h"

//----------------------------------------------------------------------
// Constructor
// Initialize class instance
//----------------------------------------------------------------------
ExposureDuration::ExposureDuration() {}

//----------------------------------------------------------------------
// Method: read
// Takes the content of the object from the binary stream
//----------------------------------------------------------------------
bool ExposureDuration::read(ifstream & fh)
{
    bool retval = true;
    for (int i = 0; i < 12; ++i) { retval &= expDuration[i].read(fh); }
    return retval;
}

//----------------------------------------------------------------------
// Method: info
// String with the representation of the object
//----------------------------------------------------------------------
string ExposureDuration::info()
{
    stringstream ss("");
    for (int i = 0; i < 12; ++i) {
	ss << i << ":" << expDuration[i].value << "ms ";
    }
    return ss.str();
}

