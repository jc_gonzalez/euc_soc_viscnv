/******************************************************************************
 * File:    ccd_id.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.CcdId
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare CcdId class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef CCDID_H
#define CCDID_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"

//==========================================================================
// Class: CcdId
// Contains fields with information on VIS RAW data compression
//==========================================================================
class CcdId {

public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
  CcdId();

public:
    uint16_t ccd_id;
    uint16_t col;
    uint16_t row;
    UInt32 data;

public:
    //----------------------------------------------------------------------
    // Method: set
    // Set the different components of the Compression Info object,
    // or the entire set of bytes at once
    void set(uint16_t accd_id, uint16_t arow, uint16_t acol,
             uint32_t thedata = 0xffffffff);

    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);

    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // CCDID_H
