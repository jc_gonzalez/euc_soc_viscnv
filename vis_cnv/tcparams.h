/******************************************************************************
 * File:    tcparams.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.TCParameters
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare TCParameters class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef TCPARAMETERS_H
#define TCPARAMETERS_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"

//==========================================================================
// Class: TCParameters
// Contains the TC Parameters information
//==========================================================================
class TCParameters {

public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
    TCParameters(int nbytes);

public:
    vector<uint8_t> tcparams;

public:
    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);

    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // TCPARAMETERS_H
