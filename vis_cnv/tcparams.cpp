/******************************************************************************
 * File:    tcparams.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.TCParameters
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement TCParameters class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "tcparams.h"

//----------------------------------------------------------------------
// Constructor
// Initialize class instance
//----------------------------------------------------------------------
TCParameters::TCParameters(int nbytes)
{
    tcparams.resize(nbytes);
}

//----------------------------------------------------------------------
// Method: read
// Takes the content of the object from the binary stream
//----------------------------------------------------------------------
bool TCParameters::read(ifstream & fh)
{
    uint8_t data;
    for (int i = 0; i < tcparams.size(); ++i) {
	fh.read((char*)&data, sizeof(data));
	tcparams[i] = data;
    }
    return true;
}

//----------------------------------------------------------------------
// Method: info
// String with the representation of the object
//----------------------------------------------------------------------
string TCParameters::info()
{
    return "[...]";
}
