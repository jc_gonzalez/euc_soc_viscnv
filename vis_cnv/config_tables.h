/******************************************************************************
 * File:    config_tables.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.ConfigTables
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare ConfigTables class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef CONFIGTABLES_H
#define CONFIGTABLES_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"

//==========================================================================
// Class: ConfigTables
// Contains:
// - Config.Sine table ID: Table version of RSU SINE table used in the
//   RSU initialisation: default=1. Table version= 0 in case of RSU
//   not initialised
// - Config.Freq. table ID: Table version of RSU FREQ table used in
//   the RSU initialisation: default=2. Table version= 0 in case of
//   RSU not initialised
// - Config.Short Freq. table ID: Table version of RSU SHORT FREQ
//   table used in the RSU initialisation: default=1. Table version= 0
//   in case of RSU not initialised
//==========================================================================
class ConfigTables {

public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
    ConfigTables();

public:
    uint16_t cfgSine;
    uint16_t cfgFreq;
    uint16_t cfgShortFreq;

public:
    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);

    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // CONFIGTABLES_H
