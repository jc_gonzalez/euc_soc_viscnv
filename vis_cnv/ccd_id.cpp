/******************************************************************************
 * File:    ccd_id.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.CcdId
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement CcdId class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "ccd_id.h"

//----------------------------------------------------------------------
// Constructor
// Initialize class instance
//----------------------------------------------------------------------
CcdId::CcdId() : ccd_id(0), col(0), row(0)
{
}

//----------------------------------------------------------------------
// Method: set
// Set the different components of the Compression Info object,
// or the entire set of bytes at once
// Parameters:
//   ccd_id       - Sets the internal parameter ccd_id
//   col - Sets the internal parameter col
//   row  - Sets the internal parameter row
//   compr_info - Sets the compount parameter (entire value)
//----------------------------------------------------------------------
void CcdId::set(uint16_t accd_id, uint16_t arow, uint16_t acol,
		uint32_t thedata)
{
    if (thedata == 0xffffffff) {
	ccd_id = accd_id;
	col = acol;
	row = arow;
	data.value = (((ccd_id & 0x0000007f) << 26) |
		      ((col & 0x00001fff) << 13) |
		      (row & 0x00001fff));
    } else {
	data.value = thedata;
	ccd_id = (thedata >> 26) & 0x0000007f;
	col = (thedata >> 13) & 0x00001fff;
	row = thedata & 0x00001fff;
    }
}

//----------------------------------------------------------------------
// Method: read
// Takes the content of the object from the binary stream
//----------------------------------------------------------------------
bool CcdId::read(ifstream & fh)
{
    data.read(fh);
    set(0, 0, 0, data.value);
    return true;
}

//----------------------------------------------------------------------
// Method: info
// String with the representation of the object
//----------------------------------------------------------------------
string CcdId::info()
{
    stringstream s("");
    s << "CCD-" << ccd_id << '_' << row << '-' << col;
    return s.str();
}
