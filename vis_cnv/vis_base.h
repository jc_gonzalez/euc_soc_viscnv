/******************************************************************************
 * File:    vis_base.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.VIS_Base
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare VIS_Base class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef VIS_BASE_H
#define VIS_BASE_H

//======================================================================
// Group: External Dependencies
//======================================================================

//----------------------------------------------------------------------
// Topic: System headers
//----------------------------------------------------------------------
#include <cstdlib>

//----------------------------------------------------------------------
// Topic: External packages
//----------------------------------------------------------------------
extern "C" {
#   include "szlib.h"
}

//----------------------------------------------------------------------
// Topic: Project headers
//----------------------------------------------------------------------
#include "types.h"

//namespace VIS {

extern const vector<string> SequenceIdName;

extern uint32_t MSK32;
extern uint32_t MSK16;
extern uint32_t MSK8;

#define TC_PARAMS_BYTES 74

// Compression/Decompression optionss mask
extern unsigned int szipOptMask;

extern unsigned int bitsPerPixel;
extern unsigned int bytesPerPixel;

// Input and Output types

#undef T

#define T_INPUT_TYPE                                                    \
    T(raw, "Binary file, compliant with ICD 4.0 draft 9"),		\
	T(raw_prev, "Binary file, compliant with ICD 4.0 previous"),	\
	T(raw_old, "Binary file, compliant with ICD 3.4"),		\
	T(le1, "LE1 VIS Product"),					\
	T(sim, "SIM VIS Product")

#define T(a, b)  IN_ ## a
enum InputType { T_INPUT_TYPE, IN_unknown=99 };
#undef T

extern vector<string> InputTypeTag;
extern map<string, string> InputTypeDesc;
extern map<string, InputType> InputTypeId;

#define T_OUTPUT_TYPE                                                   \
    T(raw, "(Uncompressed) RAW VIS Product"),				\
	T(le1, "LE1 VIS Product (quadrant based)"),			\
        T(ccd, "Each layer contains a full CDD (4 quadrants)"),         \
        T(fpa, "Full FPA")

#define T(a, b)  OUT_ ## a
enum OutputType { T_OUTPUT_TYPE, OUT_unknown=99 };
#undef T

extern vector<string> OutputTypeTag;
extern map<string, string> OutputTypeDesc;
extern map<string, OutputType> OutputTypeId;

//----------------------------------------------------------------------
// Class: VisArgs
//----------------------------------------------------------------------
class VisArgs {
public:
    string iFile;
    string oFile;
    string iType;
    string oType;
};

//----------------------------------------------------------------------
// Class: VISSize
//----------------------------------------------------------------------
class VISSize {
public:
    static const int ROWS;
    static const int COLS;
    static const int ROWS_HALF;
    static const int COLS_HALF;

    static const int ROWS_FPA;
    static const int COLS_FPA;
    static const int ROWS_QUAD;
    static const int COLS_QUAD;

    static const int QUAD_PRE_SCAN_COLS;
    static const int QUAD_OVER_SCAN_COLS;

    static const int CHARGE_INJEC_STRUCT_ROWS;
    static const int QUAD_CHARGE_INJEC_STRUCT_ROWS;

    static const int CCD_ROWS_IN_FPA;
    static const int CCD_COLS_IN_FPA;
};

//----------------------------------------------------------------------
// Class: SpaceWirePacketTag
// A start/end of packet tag
//----------------------------------------------------------------------
class SpaceWirePacketTag : public UInt32 {
public:
    SpaceWirePacketTag() {}
public:
    bool is(uint32_t expectedContent) { return expectedContent == value; }
    void rollback(ifstream & fh) { fh.seekg(-sizeof(data), fh.cur); }
};

//----------------------------------------------------------------------
// Class: OperationID
// Teh (composed) operation ID
//----------------------------------------------------------------------
class OperationID : public UInt32 {};

//----------------------------------------------------------------------
// Class: CRC16
// A 16-bit computed CRC
//----------------------------------------------------------------------
class CRC16 : public UInt16 {};

//----------------------------------------------------------------------
// Class: MJDate
// A 6-byte definition of the MJD
//----------------------------------------------------------------------
class MJDate : public UInt48 {};

//}

#endif // VIS_BASE_H
