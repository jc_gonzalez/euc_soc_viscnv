/******************************************************************************
 * File:    ver_seqconf.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.VersionSeqConf
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare VersionSeqConf class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef VERSIONSEQCONF_H
#define VERSIONSEQCONF_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"


//==========================================================================
// Class: VersionSeqConf
// Contains the configuration table version for the ref. ROE sequence
//==========================================================================
class VersionSeqConf {

public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
    VersionSeqConf();

public:
    UInt16 verSeqConf[12];

public:
    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);

    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // VERSIONSEQCONF_H
