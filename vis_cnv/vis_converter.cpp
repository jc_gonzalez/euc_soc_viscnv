/******************************************************************************
 * File:    vis_converter.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.VIS_Converter
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement VIS_Converter class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "vis_converter.h"

#include <functional>

extern "C" {
#   include "fitsio.h"
}

//namespace VIS {

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
VIS_Converter::VIS_Converter() :
    logger(Logging::getLogger("viscnv"))
{
    int i;
    int n = 0;

    // Compute CCD Coords <-> Img.Ext.number tables
    for (int row = 1; row <= 6; ++row) {
        for (int col = 1; col <= 6; ++col) {
            for (int q = 1; q <= 4; ++q) {
                i = ccdCoord2ImgExtNumber(row, col, q);
                auto newElem = std::pair<int, CCDCoord>(i, CCDCoord(row, col, q, n, i));
                imgExt2CcdCoord.insert(newElem);
            }
            ++n;
            ntoi.insert(std::pair<int, int>(n, i));
        }
    }
    /*
    for (auto & kv : imgExt2CcdCoord) {
        auto & cc = kv.second;
        std::cout << cc.row << ", " << cc.col << " - " << cc.quad << " : "
                  << cc.nccd << " - " << cc.imgIdx << '\n';
    }
    */
}

//----------------------------------------------------------------------
// Function: fileSize
// Returns the file size in bytes
//----------------------------------------------------------------------
long VIS_Converter::fileSize(std::string filename, std::ifstream::pos_type & token)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    token = in.tellg();
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

//----------------------------------------------------------------------
// Function: ccdCoord2ImgExtNumber
// Computes the image extension number from the quadrant coordinates
//----------------------------------------------------------------------
int VIS_Converter::ccdCoord2ImgExtNumber(int row, int col, int quad)
{
    return ((row - 1) * 24 + (6 - col) * 4 + quad +
            ((col >= 4) ? 0 : ((quad <= 2) ? +2 : -2)));
}

//----------------------------------------------------------------------
// Function: run
// Launches the entire retrieving and generation process
//----------------------------------------------------------------------
bool VIS_Converter::checkArgs(VisArgs & someArgs)
{
    // Take arguments
    args = someArgs;

    inputFileName = args.iFile;
    outputFileName = args.oFile;

    // Check that input file exists
    struct stat st;
    bool exists = (stat(inputFileName.c_str(), &st) == 0);
    if (! exists) {
        logger.error("ERROR: Input file " + inputFileName + " not found!");
        return false;
    }

    // Check input file type
    auto iTypeIt = InputTypeId.find(args.iType);
    if (iTypeIt == InputTypeId.end()) {
        logger.error("ERROR: Input file type '" + args.iType + "' not supported!");
        return false;
    }
    inputType = iTypeIt->second;

    // Check input file type
    auto oTypeIt = OutputTypeId.find(args.oType);
    if (oTypeIt == OutputTypeId.end()) {
        logger.error("ERROR: Output file type '" + args.oType + "' not supported!");
        return false;
    }
    outputType = oTypeIt->second;

    return true;
}

//----------------------------------------------------------------------
// Function: run
// Launches the entire retrieving and generation process
//----------------------------------------------------------------------
bool VIS_Converter::run(VisArgs & someArgs)
{
    bool retval = checkArgs(someArgs);

    if (!retval) { return false; }

    // Create appropriate input objects
    switch (inputType) {
    case IN_raw:
        phdr = new RAW_VIS_Header;
        break;
    case IN_raw_prev:
        phdr = new RAW_VIS_Header_prev;
        break;
    case IN_raw_old:
        phdr = new RAW_VIS_Header_old;
        break;
    default:
	logger.error(fmt("Input file format $ not yet supported!", args.iType));
	return false;
        break;
    }
    RAW_VIS_Header_base & hdr = *phdr;

    RAW_VIS_SciPacket sciPkt;

    SpaceWirePacketTag tag;

    int roePackets = 0;
    int sciPackets = 0;
    int sciPacketsROE = 0;
    vector<int> sciPacketsPerROE;

    ccdIdx = -1;

    bool reorder = true;

    std::ifstream::pos_type endToken;
    long fSize = fileSize(args.iFile.c_str(), endToken);
    ifstream ifHdl(args.iFile, std::ifstream::binary);

    while (ifHdl.tellg() != endToken) {

        tag.read(ifHdl);

        if (tag.is(SpaceWireROEPacketHeaderTag)) {

            tag.rollback(ifHdl);
            hdr.read(ifHdl);
            sciPkt.setCompressionInfo(hdr.comprInfo);
            logger.info("\n" + hdr.info());
            ++roePackets;
            if (sciPacketsROE > 0) {
                sciPacketsPerROE.push_back(sciPacketsROE);
                sciPacketsROE = 0;
            }

        } else if (tag.is(SpaceWireSciPacketHeaderTag)) {

            tag.rollback(ifHdl);
            sciPkt.read(ifHdl);
            //logger.info("\n" + sciPkt.info());
            ++sciPackets;
            ++sciPacketsROE;

            if (sciPkt.ccd.ccd_id != ccdIdx) {
                if (ccdIdx != -1) { ccds.push_back(ccd); }
                ccdIdx = sciPkt.ccd.ccd_id;
                logger.info(fmt("CCD #$", ccdIdx + 1));
                ccd.reset();
            }

            ccd.storeDataAtPos((hdr.comprInfo.compr_type == NO_COMPR) ?
                               sciPkt.data : sciPkt.uncomprPixels,
                               sciPkt.ccd.row, sciPkt.ccd.col,
                               hdr.comprInfo.compr_type == WITH_REORDER_121);

        } else {

            logger.error(fmt("ERROR: Unknown tag: $", tag.value));
            return false;

        }
    }

    sciPacketsPerROE.push_back(sciPacketsROE);
    ccds.push_back(ccd);

    logger.info("File " + inputFileName + ": ");
    logger.info(fmt("Size (byes):            $ ($)", fSize, endToken));
    logger.info(fmt("ROE Header packets:     $", roePackets));
    logger.info(fmt("Science Header packets: $", sciPackets));
    std::stringstream ss("Sci.Packets per ROE:    [ ");
    for (unsigned int i = 0; i < sciPacketsPerROE.size(); ++i) {
        ss << sciPacketsPerROE.at(i) << ' ';
    }
    ss << ']';
    logger.info(ss.str());
    logger.info(fmt("CCDs:                   $", ccds.size()));

    retval = createFits();

    if (retval) logger.info("Done.");
    return retval;
}

//----------------------------------------------------------------------
// Function: createFits
// Create a FITS file with the content of the input file as specified
//----------------------------------------------------------------------
bool VIS_Converter::createFits()
{
    auto fitsCreatorTable =
        make_array(std::mem_fn(&VIS_Converter::createRawBin),
		   std::mem_fn(&VIS_Converter::createLe1Fits),
                   std::mem_fn(&VIS_Converter::createCcdFits),
                   std::mem_fn(&VIS_Converter::createFpaFits));

    auto fitsCreator = fitsCreatorTable[(int)(outputType)];

    return fitsCreator(*this);
}

//----------------------------------------------------------------------
// Function: createCcdFits
// Create a FITS file with the content of entire CCDs as image extensions
//----------------------------------------------------------------------
bool VIS_Converter::createCcdFits()
{
    // Create CCD FITS file

    logger.info("Creating CCD FITS file " + outputFileName + " . . .");

    fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    string keywdName, sKeywd;
    long fpixel = 1, naxis = 2, nelements, lKeywd;
    long naxes[2] = {VISSize::COLS, VISSize::ROWS};
    
    status = 0;         /* initialize status before calling fitsio routines */
    fits_create_file(&fptr, outputFileName.c_str(), &status);   /* create new file */
    if (status != 0) return false;
    
    fits_create_img(fptr, USHORT_IMG, 0, 0, &status);
    if (status != 0) return false;
    
    /* Write a keyword; must pass the ADDRESS of the value */
    keywdName = "EXPOSURE", lKeywd = 0;
    fits_update_key(fptr, TLONG, keywdName.c_str(), &lKeywd, "Total Exposure Time", &status);
    logger.info(fmt("Writing keyword: $=$ : $", keywdName, lKeywd,
                    (status == 0 ? "OK" : "FAILED!")));

    nelements = naxes[0] * naxes[1];          /* number of pixels to write */

    for (unsigned int nccd = 0; nccd < ccds.size(); ++nccd) {

        int & i = ntoi[nccd + 1];
        auto & cc = imgExt2CcdCoord.find(i)->second;

        LE1_VIS_CCD & ccd = ccds.at(nccd);
        
        // Create the array image (16-bit short integer pixels
        fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
        logger.info(fmt("Creation of image $: $", nccd,
			 (status == 0 ? "OK" : "FAILED!")));

        // Write the array of integers to the image
        fits_write_img(fptr, TUSHORT, fpixel, nelements, ccd.img[0], &status);
        logger.info(fmt("Writing image $: $", nccd,
                        (status == 0 ? "OK" : "FAILED!")));

        // Write a keywords
        keywdName = "EXTNAME", sKeywd = fmt("CCD $-$", cc.row, cc.col);
        fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "Extension descriptor", &status);
        logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
			 (status == 0 ? "OK" : "FAILED!")));

        keywdName = "CCDID", sKeywd = fmt("$-$", cc.row, cc.col);
        fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "CCD Identifier", &status);
        logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
			 (status == 0 ? "OK" : "FAILED!")));

    }

    fits_close_file(fptr, &status);            /* close the file */
    fits_report_error(stderr, status);  /* print out any error messages */

    return true;
}


//----------------------------------------------------------------------
// Function: createLe1Fits
// Create a FITS file with the LE1 format (1 quad per image extension)
//----------------------------------------------------------------------
bool VIS_Converter::createLe1Fits()
{
    // Create LE1 FITS file

    logger.info("Creating LE1 FITS file " + outputFileName + " . . .");

    fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj, nccd;
    string keywdName, sKeywd;
    long fpixel = 1, naxis = 2, nelements, lKeywd;
    long naxes[2] = {VISSize::COLS_QUAD, VISSize::ROWS_QUAD};

    uint16_t * img = new uint16_t [VISSize::ROWS_QUAD * VISSize::COLS_QUAD];
    uint16_t ** qimg = new uint16_t * [VISSize::ROWS_QUAD];
    for (int r = 0; r < VISSize::ROWS_QUAD; ++r) {
        qimg[r] = &(img[r * VISSize::COLS_QUAD]);
    }

    const char * squads = " EFGH";
    string oFile = "!" + outputFileName;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_create_file(&fptr, oFile.c_str(), &status);   /* create new file */
    if (status != 0) return false;
    
    fits_create_img(fptr, USHORT_IMG, 0, 0, &status);
    if (status != 0) return false;

    /* Write a keyword; must pass the ADDRESS of the value */
    keywdName = "EXPOSURE", lKeywd = 0;
    fits_update_key(fptr, TLONG, keywdName.c_str(), &lKeywd, "Total Exposure Time", &status);
    logger.info(fmt("Writing keyword: $=$ : $:$", keywdName, lKeywd,
                    status, (status == 0 ? "OK" : "FAILED!")));

    nelements = naxes[0] * naxes[1];          /* number of pixels to write */

    for (unsigned int imgExt = 1; imgExt <= imgExt2CcdCoord.size(); ++imgExt) {

        auto & cc = imgExt2CcdCoord.find(imgExt)->second;
        nccd = cc.nccd;

        /* Create the primary array image (16-bit short integer pixels */
        fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
        char squad = squads[cc.quad];
        logger.info(fmt("Creation of image $ ($.$): $", imgExt, cc.nccd, cc.quad,
			 (status == 0 ? "OK" : "FAILED!")));

        switch (cc.quad) {
        case 1: // E
            for (int i = 0; i < VISSize::ROWS_QUAD; ++i) {
                for (int j = 0; j < VISSize::COLS_QUAD; ++j) {
                    qimg[i][j] = ccds.at(nccd).img[i][j];
                }
            }
            break;
        case 2: // F
            for (int i = 0; i < VISSize::ROWS_QUAD; ++i) {
                for (int j = 0; j < VISSize::COLS_QUAD; ++j) {
                    qimg[i][j] = ccds.at(nccd).img[VISSize::ROWS - 1 - i][j];
                }
            }
            break;
        case 3: // G
            for (int i = 0; i < VISSize::ROWS_QUAD; ++i) {
                for (int j = 0; j < VISSize::COLS_QUAD; ++j) {
                    qimg[i][j] = ccds.at(nccd).img[VISSize::ROWS - 1 - i][VISSize::COLS - 1 - j];
                }
            }
            break;
        case 4: // H
            for (int i = 0; i < VISSize::ROWS_QUAD; ++i) {
                for (int j = 0; j < VISSize::COLS_QUAD; ++j) {
                    qimg[i][j] = ccds.at(nccd).img[i][VISSize::COLS - 1 - j];
                }
            }
            break;
        default:
            continue;
        }

        /* Write the array of integers to the image */
        fits_write_img(fptr, TUSHORT, fpixel, nelements, img, &status);
        logger.info(fmt("Writing image $: $", imgExt,
			 (status == 0 ? "OK" : "FAILED!")));

        // Write a keywords
        keywdName = "EXTNAME", sKeywd = fmt("Q$ $-$.$", imgExt, cc.row, cc.col, squad);
        fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "Extension descriptor", &status);
        logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
			 (status == 0 ? "OK" : "FAILED!")));

        keywdName = "CCDID", sKeywd = fmt("$-$", cc.row, cc.col);
        fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "CCD Identifier", &status);
        logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
			 (status == 0 ? "OK" : "FAILED!")));

        keywdName = "QUADRANT", sKeywd = squad;
        fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "Quadrant identifier", &status);
        logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
			 (status == 0 ? "OK" : "FAILED!")));

    }

    // Clean up
    delete [] qimg;
    delete [] img;
    
    fits_close_file(fptr, &status);            /* close the file */
    fits_report_error(stderr, status);  /* print out any error messages */

    return true;
}


//----------------------------------------------------------------------
// Function: createFpaFits
// Create a FITS file with the content of the entire FPA as one image
//----------------------------------------------------------------------
bool VIS_Converter::createFpaFits()
{
    // Create FPA FITS file

    logger.info("Creating CCD FITS file " + outputFileName + " . . .");

    fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    string keywdName, sKeywd;
    long fpixel = 1, naxis = 2, lKeywd;
    const long NCols = VISSize::COLS * VISSize::CCD_COLS_IN_FPA;
    const long NRows = VISSize::ROWS * VISSize::CCD_ROWS_IN_FPA;
    long naxes[2] = {NCols, NRows};

    const long nelements = naxes[0] * naxes[1];          /* number of pixels to write */

    uint16_t * img = new uint16_t [nelements];
    uint16_t ** qimg = new uint16_t * [NRows];
    for (int r = 0; r < NRows; ++r) {
        qimg[r] = &(img[r * NCols]);
    }

    status = 0;         /* initialize status before calling fitsio routines */
    fits_create_file(&fptr, outputFileName.c_str(), &status);   /* create new file */
    if (status != 0) return false;
    
    fits_create_img(fptr, USHORT_IMG, 0, 0, &status);
    if (status != 0) return false;

    /* Write a keyword; must pass the ADDRESS of the value */
    keywdName = "EXPOSURE", lKeywd = 0;
    fits_update_key(fptr, TLONG, keywdName.c_str(), &lKeywd, "Total Exposure Time", &status);
    logger.info(fmt("Writing keyword: $=$ : $", keywdName, lKeywd,
                    (status == 0 ? "OK" : "FAILED!")));

    // Create the array image (16-bit short integer pixels
    fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
    logger.info(fmt("Creation of FPA . . . $", 
                    (status == 0 ? "OK" : "FAILED!")));

    unsigned int nccd = 0;
    unsigned int r0 = 0;
    for (unsigned int r = 0; r < VISSize::CCD_ROWS_IN_FPA; ++r) {
        unsigned int c0 = 0;
        for (unsigned int c = 0; c < VISSize::CCD_COLS_IN_FPA; ++c) {

            LE1_VIS_CCD & ccd = ccds.at(nccd);
            logger.info(fmt("Storing CCD #$ at FPA image, at pos ($, $). . .", nccd + 1, r0, c0));

            for (int i = 0; i < VISSize::ROWS; ++i) {
                for (int j = 0; j < VISSize::COLS; ++j) {
                    qimg[r0 + i][c0 + j] = ccd.img[i][j];
                }
            }

            ++nccd;
            c0 += VISSize::COLS;
        }
        r0 += VISSize::ROWS;
    }

    // Write the array of integers to the image
    fits_write_img(fptr, TUSHORT, fpixel, nelements, img, &status);
    logger.info(fmt("Writing image $: $", nccd,
                    (status == 0 ? "OK" : "FAILED!")));

    // Write a keywords
    keywdName = "EXTNAME", sKeywd = "FPA";
    fits_update_key(fptr, TSTRING, keywdName.c_str(), &sKeywd, "Extension descriptor", &status);
    logger.info(fmt("Writing keyword: $=$ : $", keywdName, sKeywd,
                    (status == 0 ? "OK" : "FAILED!")));

    // Clean up
    delete [] qimg;
    delete [] img;
    
    fits_close_file(fptr, &status);            /* close the file */
    fits_report_error(stderr, status);  /* print out any error messages */

    return true;
}

//----------------------------------------------------------------------
// Function: createRawBin
// Create a binary file with the file of the RAW generated files
//----------------------------------------------------------------------
bool VIS_Converter::createRawBin()
{
    return true;
}

//}
