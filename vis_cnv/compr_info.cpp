/******************************************************************************
 * File:    compr_info.cpp
 *          This file is part of Tools
 *
 * Domain:  VISConverter.CompressionInfo
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement CompressionInfo class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "compr_info.h"

//----------------------------------------------------------------------
// Constructor
// Initialize class instance
//----------------------------------------------------------------------
CompressionInfo::CompressionInfo() :
    mode(SCIENCE), compr_type(NO_COMPR), compr_prs(0)
{
}

//----------------------------------------------------------------------
// Method: set
// Set the different components of the Compression Info object,
// or the entire set of bytes at once
// Parameters:
//   mode       - Sets the internal parameter mode
//   compr_type - Sets the internal parameter compr_type
//   compr_prs  - Sets the internal parameter compr_prs
//   compr_info - Sets the compount parameter (entire value)
//----------------------------------------------------------------------
void CompressionInfo::set(ComprMode amode, ComprType acompr_type,
			  uint16_t acompr_prs, uint16_t acompr_info)
{
    if (acompr_info == 0xffff) {
	mode = amode;
	compr_type = acompr_type;
	compr_prs = acompr_prs;
	compr_info.value = (((int(mode) & 0x00000003) << 14) |
			    ((int(compr_type) & 0x00000003) << 12) |
			    (compr_prs & 0x00000fff));
    } else {
	compr_info.value = acompr_info;
	mode = ComprMode((compr_info.value >> 14) & 0x00000003);
	compr_type = ComprType((compr_info.value >> 12) & 0x00000003);
	compr_prs = compr_info.value & 0x00000fff;
    }
}

//----------------------------------------------------------------------
// Method: read
// Takes the content of the object from the binary stream
//----------------------------------------------------------------------
bool CompressionInfo::read(ifstream & fh)
{
    compr_info.read(fh);
    set(ComprMode(0), ComprType(0), 0, compr_info.value);
    return true;
}

//----------------------------------------------------------------------
// Method: info
// String with the representation of the object
//----------------------------------------------------------------------
string CompressionInfo::info()
{
    static vector<string> comprTypeStr = {"No Compr",
					  "121 Without reordering",
					  "121 With reordering", ""};
    static char buffer[MAX_TMP_STR];

    sprintf(buffer, "%04X = Mode: %d - %s, ComprType: %d - %s, "
	    "ComprPrs: %d px per block",
	    compr_info.value,
	    mode, (mode == SCIENCE) ? "SCIENCE" : "MANUAL",
	    compr_type, comprTypeStr[int(compr_type)].c_str(), compr_prs);
    return string(buffer);
}

