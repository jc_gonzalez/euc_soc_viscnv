
/******************************************************************************
 * File:    compr_info.h
 *          This file is part of Tools
 *
 * Domain:  VISConverter.CompressionInfo
 *
 * Last update:  1.0
 *
 * Date:    20191126
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare CompressionInfo class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef COMPRESSIONINFO_H
#define COMPRESSIONINFO_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
// - types.h
//------------------------------------------------------------
#include "types.h"

//----------------------------------------------------------------------
// Enum: ComprMode
// Compression Mode: 0: Science, 1: Manual
//----------------------------------------------------------------------
enum ComprMode {
    SCIENCE = 0,
    MANUAL = 1
};

//----------------------------------------------------------------------
// Enum: ComprType
// Compression Type:
// 0: No Compr
// 1: 121 Without reordering
// 2: 121 With reordering
//----------------------------------------------------------------------
enum ComprType {
    NO_COMPR = 0,
    WITHOUT_REORDER_121 = 1,
    WITH_REORDER_121 = 2,
};

//==========================================================================
// Class: CompressionInfo
// Contains fields with information on VIS RAW data compression
//==========================================================================
class CompressionInfo {
public:
    //----------------------------------------------------------------------
    // Constructor
    // Initialize class instance
    //----------------------------------------------------------------------
    CompressionInfo();

public:
    ComprMode mode;
    ComprType compr_type;
    uint16_t compr_prs;
    UInt16 compr_info;

public:
    //----------------------------------------------------------------------
    // Method: set
    // Set the different components of the Compression Info object,
    // or the entire set of bytes at once
    // Parameters:
    //   mode       - Sets the internal parameter mode
    //   compr_type - Sets the internal parameter compr_type
    //   compr_prs  - Sets the internal parameter compr_prs
    //   compr_info - Sets the compount parameter (entire value)
    //----------------------------------------------------------------------
    void set(ComprMode amode, ComprType acompr_type,
	     uint16_t acompr_prs, uint16_t acompr_info = 0xffff);

    //----------------------------------------------------------------------
    // Method: read
    // Takes the content of the object from the binary stream
    //----------------------------------------------------------------------
    bool read(ifstream & fh);
  
    //----------------------------------------------------------------------
    // Method: info
    // String with the representation of the object
    //----------------------------------------------------------------------
    string info();
};

#endif // COMPRESSIONINFO_H
